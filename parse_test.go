package applause_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/typo/pointer"
)

func ExampleParser_AddStruct() {
	type S struct {
		Bool    bool     `option:"bool|b"`
		Int     int      `option:""`
		List    []string `option:"list"`
		NotUsed int
		String  string `option:"string|s"`
	}

	var (
		ctx    = context.Background()
		parser applause.Parser
		s      = new(S)
	)

	parser.AddStruct(s)

	operands, err := parser.Parse(ctx, []string{
		"--bool",
		"--string", "string’s value",
		"op1",
		"--int", "42",
		"--list", "first",
		"--list", "second",
		"op2",
	})
	if err != nil {
		panic(err)
	}

	fmt.Printf(
		`
struct:
  Bool:    %+v
  Int:     %+v
  List:    %+v
  NotUsed: %+v
  String:  %+v
operands:  %+v
`,
		s.Bool,
		s.Int,
		s.List,
		s.NotUsed,
		s.String,
		operands,
	)

	// Output:
	// struct:
	//   Bool:    true
	//   Int:     42
	//   List:    [first second]
	//   NotUsed: 0
	//   String:  string’s value
	// operands:  [op1 op2]
}

func Example_embedded() {
	type A struct {
		StringA string `option:"a"`
	}

	type B struct {
		StringB string `option:"b"`
	}

	type C struct {
		// A is embedded and so are its options.
		A

		// B is embedded but its options aren’t.
		B `option:"-"`

		StringC string `option:"c"`
	}

	var (
		c      = new(C)
		ctx    = context.Background()
		parser applause.Parser
	)

	parser.AddStruct(c)

	if _, err := parser.Parse(ctx, []string{
		"-a", "foo",
		"-c", "bar",
	}); err != nil {
		panic(err)
	}

	fmt.Printf("StringA=%q\nStringC=%q\n", c.StringA, c.StringC)

	_, err := parser.Parse(ctx, []string{
		"-b", "foo",
	})
	if err == nil {
		panic("should return error")
	}

	fmt.Printf("-b is embedded but ignored, thus: %v", err)

	// Output:
	// StringA="foo"
	// StringC="bar"
	// -b is embedded but ignored, thus: unknown option: -b
}

func Example_increment_decrement() {
	var (
		ctx       = context.Background()
		parser    applause.Parser
		verbosity int
	)

	parser.Add(applause.Option{
		Target: &verbosity,
		Names:  []string{"v"},
		Add:    +1,
	})
	parser.Add(applause.Option{
		Target: &verbosity,
		Names:  []string{"q"},
		Add:    -1,
	})

	if _, err := parser.Parse(ctx, []string{"-vv", "-q", "-v"}); err != nil {
		panic(err)
	}

	fmt.Printf("verbosity: %v", verbosity)

	// Output:
	// verbosity: 2
}

func Example_toggle() {
	var (
		ctx    = context.Background()
		option = true
	)

	var parser applause.Parser

	// `!` add `--no-option` as well as `--option`
	parser.Add(applause.Option{
		Target: &option,
		Names:  []string{"option"},
		Negate: true,
	})

	if _, err := parser.Parse(ctx, []string{"--no-option"}); err != nil {
		panic(err)
	}

	fmt.Printf("option: %v", option)

	// Output:
	// option: false
}

func TestParse(t *testing.T) {
	t.Parallel()

	type Values struct {
		Bool    bool
		BoolP   **bool
		Counter uint64
		Int     int
		Map     map[int]float64
		Slice   []string
		String  string
	}

	var (
		ctx = context.Background()
		v   Values
	)

	for _, c := range [...]struct {
		Name             string
		Options          []applause.Option
		Args             []string
		Error            error
		Panic            string
		ExpectedValues   Values
		ExpectedOperands []string
	}{
		{
			Name: "nil target",
			Options: []applause.Option{{
				Names: []string{"nil", "n"},
			}},
			Panic: `invalid target <nil>`,
		},

		{
			Name: "no operands",
		},

		{
			Name: "no operands (--)",
			Args: []string{"--"},
		},

		{
			Name:             "just operands",
			Args:             []string{"op1", "op2", "--", "--op3"},
			ExpectedOperands: []string{"op1", "op2", "--op3"},
		},

		{
			Name: "extra value",
			Options: []applause.Option{{
				Target: &v.Bool,
				Names:  []string{"bool"},
			}},
			Args:  []string{"--bool=value"},
			Error: applause.ErrInvalidValue,
		},

		{
			Name: "append slice",
			Options: []applause.Option{{
				Target: &v.Slice,
				Names:  []string{"slice"},
			}},
			Args:           []string{"--slice", "foo", "--slice", "bar"},
			ExpectedValues: Values{Slice: []string{"foo", "bar"}},
		},

		{
			Name: "map",
			Options: []applause.Option{{
				Target: &v.Map,
				Names:  []string{"map"},
			}},
			Args: []string{"--map", "42=3.14"},
			ExpectedValues: Values{
				Map: map[int]float64{
					42: 3.14,
				},
			},
		},

		{
			Name: "map; explicit",
			Options: []applause.Option{{
				Target: &v.Map,
				Names:  []string{"map"},
			}},
			Args: []string{"--map=42=3.14"},
			ExpectedValues: Values{
				Map: map[int]float64{
					42: 3.14,
				},
			},
		},

		{
			Name: "map; missing value",
			Options: []applause.Option{{
				Target: &v.Map,
				Names:  []string{"map"},
			}},
			Args: []string{"--map", "42"},
			ExpectedValues: Values{
				Map: map[int]float64{
					42: 0,
				},
			},
		},

		{
			Name: "misc.",
			Options: []applause.Option{
				{
					Target: &v.String,
					Names:  []string{"string", "s"},
				},
				{
					Target: &v.Int,
					Names:  []string{"int", "i"},
				},
				{
					Target: &v.Bool,
					Names:  []string{"bool", "b"},
					Negate: true,
				},
				{
					Names: []string{"func", "f"},
					Target: func(_ context.Context, _, _ string) error {
						v.Counter++

						return nil
					},
				},
			},
			Args: []string{
				"--string", "string’s value",
				"op1",
				"--bool",
				"op2",
				"--func",
				"test",
				"-i42",
				"--",
				"--op3",
			},
			ExpectedValues: Values{
				Bool:    true,
				Counter: 1,
				Int:     42,
				String:  "string’s value",
			},
			ExpectedOperands: []string{"op1", "op2", "--op3"},
		},

		{
			Name: "func(string)",
			Options: []applause.Option{
				{
					Names: []string{"func", "f"},
					Target: func(_ context.Context, _, value string) error {
						v.String = value
						return nil
					},
				},
			},
			Args: []string{"--func", "func’s value"},
			ExpectedValues: Values{
				String: "func’s value",
			},
		},

		{
			Name: "bool pointer",
			Options: []applause.Option{{
				Target: &v.BoolP,
				Names:  []string{"bool", "b"},
				Negate: true,
			}},
			Args:           []string{"--bool"},
			ExpectedValues: Values{BoolP: pointer.To(pointer.To(true))},
		},

		{
			Name: "bool pointer, negate",
			Options: []applause.Option{{
				Target: &v.BoolP,
				Names:  []string{"bool", "b"},
				Negate: true,
			}},
			Args:           []string{"--bool", "--no-bool"},
			ExpectedValues: Values{BoolP: nil},
		},

		{
			Name: "true, false",
			Options: []applause.Option{{
				Target: &v.Bool,
				Names:  []string{"bool", "b"},
				Negate: true,
			}},
			Args:           []string{"--bool", "--no-bool"},
			ExpectedValues: Values{Bool: false},
		},

		{
			Name: "true, false, true",
			Options: []applause.Option{{
				Target: &v.Bool,
				Names:  []string{"bool", "b"},
				Negate: true,
			}},
			Args:           []string{"--bool", "--no-bool", "--bool"},
			ExpectedValues: Values{Bool: true},
		},

		{
			Name: "bundle; next arg value",
			Options: []applause.Option{
				{
					Target: &v.Bool,
					Names:  []string{"b"},
				},
				{
					Target: &v.String,
					Names:  []string{"s"},
				},
			},
			Args: []string{"-bs", "foo"},
			ExpectedValues: Values{
				Bool:   true,
				String: "foo",
			},
		},

		{
			Name: "bundle; missing value",
			Options: []applause.Option{
				{
					Target: &v.Bool,
					Names:  []string{"b"},
				},
				{
					Target: &v.String,
					Names:  []string{"s"},
				},
			},
			Args:  []string{"-bs"},
			Error: applause.ErrNoValue,
		},

		{
			Name: "bundle; tight value",
			Options: []applause.Option{
				{
					Target: &v.Bool,
					Names:  []string{"b"},
				},
				{
					Target: &v.String,
					Names:  []string{"s"},
				},
			},
			Args: []string{"-bsfoo"},
			ExpectedValues: Values{
				Bool:   true,
				String: "foo",
			},
		},

		{
			Name: "bundle; explicit value",
			Options: []applause.Option{
				{
					Target: &v.Bool,
					Names:  []string{"b"},
				},
				{
					Target: &v.String,
					Names:  []string{"s"},
				},
			},
			Args: []string{"-bs=foo"},
			ExpectedValues: Values{
				Bool:   true,
				String: "foo",
			},
		},

		{
			Name: "bundle; extra value",
			Options: []applause.Option{
				{
					Target: &v.Bool,
					Names:  []string{"b"},
				},
				{
					Target: &v.String,
					Names:  []string{"s"},
				},
			},
			Args:  []string{"-b=foo"},
			Error: applause.ErrInvalidValue,
		},

		{
			Name: "increment",
			Options: []applause.Option{{
				Target: &v.Int,
				Names:  []string{"i"},
				Add:    +1,
			}},
			Args:           []string{"-i", "-i"},
			ExpectedValues: Values{Int: 2},
		},

		{
			Name: "unknown option",
			Options: []applause.Option{{
				Target: &v.Int,
				Names:  []string{"i"},
			}},
			Args:  []string{"--unknown"},
			Error: applause.ErrUnknownOption,
		},

		{
			Name: "parse failure",
			Options: []applause.Option{{
				Target: &v.Int,
				Names:  []string{"i"},
			}},
			Args:  []string{"-i", "not an int"},
			Error: applause.ErrInvalidValue,
		},

		{
			Name: "increment, but then set explicitly",
			Options: []applause.Option{{
				Target: &v.Int,
				Names:  []string{"i"},
				Add:    +1,
			}},
			Args:           []string{"-i", "-i", "-i=42"},
			ExpectedValues: Values{Int: 42},
		},

		{
			Name: "decrement",
			Options: []applause.Option{{
				Target: &v.Int,
				Names:  []string{"i"},
				Add:    -1,
			}},
			Args:           []string{"-i", "-i"},
			ExpectedValues: Values{Int: -2},
		},
	} {
		c := c

		t.Run(c.Name, func(t *testing.T) {
			var parser applause.Parser
			v = Values{}

			var f func(t testing.TB, f assert.PanicTestFunc)

			if c.Panic == "" {
				f = func(t testing.TB, f assert.PanicTestFunc) {
					t.Helper()
					assert.NotPanics(t, f)
				}
			} else {
				f = func(t testing.TB, f assert.PanicTestFunc) {
					t.Helper()
					assert.PanicsWithValue(t, c.Panic, f)
				}
			}

			f(t, func() {
				parser.Add(c.Options...)
			})

			operands, err := parser.Parse(ctx, c.Args)

			if c.Error == nil {
				require.NoError(t, err)

				assert.EqualValues(t, c.ExpectedValues, v)
				assert.ElementsMatch(t, c.ExpectedOperands, operands)

				return
			}

			assert.Empty(t, operands)
			if assert.Error(t, err) {
				assert.ErrorIs(t, err, c.Error)
			}
		})
	}
}
