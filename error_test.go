package applause_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/biffen/go-applause"
)

func TestError(t *testing.T) {
	t.Parallel()

	assert.Equal(t, fmt.Sprintf("%v", applause.Error("foo")), "foo")
}
