package internal

import (
	"regexp"
	"strings"
)

func ParseName(str string) (*string, *regexp.Regexp) {
	if str == "" {
		panic("Empty name")
	}

	if len(str) >= 2 && strings.HasPrefix(str, "/") &&
		strings.HasSuffix(str, "/") {
		r, err := regexp.Compile("^" + str[1:len(str)-1] + "$")
		if err != nil {
			return nil, nil
		}

		return nil, r
	}

	return &str, nil
}
